# I-Ching Oracle Guide App 

This application is an index card deck of hexagrams delivered by Ionic Framework with Vue.

## Prerequisites

- [Node.js](https://nodejs.org/en/) (lts/fermium)
- [Vue.js](https://v3.vuejs.org/guide/)
- [Ionic CLI](https://ionicframework.com/docs/intro/cli)

## Getting Started

* [Download the installer](https://nodejs.org/) for Node LTS.
* Clone this repository 
* Run `npm install` from the project root.
* Run `npm run serve` in a terminal from the project root.

