// https://docs.cypress.io/api/introduction/api.html

describe('My First Test', () => {
  it('Visits the app root url', () => {
    cy.visit('http://localhost:8080')
    cy.contains('.title', 'I-Ching')
  })
  it('Presses the start button', () => {
    cy.visit('http://localhost:8080')
    cy.contains('.start-button', 'Press Start').click()
    cy.contains('.sub-title', '1. Relax') 
  })
  it('Presses the previous button', () => {
    cy.visit('http://localhost:8080')
    cy.contains('.start-button', 'Press Start').click()
    cy.contains('.start-button', 'Reveal Answer').click()
    cy.get('#num').then(($span) => {
      // capture what num is right now
      const num1 = parseFloat($span.text())

      cy.get('.previous')
	.click()
	.then(() => {
          // now capture it again
          const num2 = parseFloat($span.text())
          
          // make sure it's what we expected
          expect(num2).to.eq(num1 - 1)
	})
    })
  })
  it('Presses the next button', () => {
    cy.visit('http://localhost:8080')
    cy.contains('.start-button', 'Press Start').click()
    cy.contains('.start-button', 'Reveal Answer').click()
    cy.get('#num').then(($span) => {
      // capture what num is right now
      const num1 = parseFloat($span.text())

      cy.get('.next')
	.click()
	.then(() => {
          // now capture it again
          const num2 = parseFloat($span.text())
          
          // make sure it's what we expected
          expect(num2).to.eq(num1 + 1)
	})
    })
  })
  it('Presses the consult button', () => {
    cy.visit('http://localhost:8080')
    cy.contains('.start-button', 'Press Start').click()
    cy.contains('.start-button', 'Reveal Answer').click()
    cy.contains('.instructions', 'Tap to Reveal Meaning').click()
    cy.contains('.consult', 'Consult').click()
    cy.contains('.sub-title', '1. Relax')   
  })
})
